<?php

use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
use Phalcon\Http\Response;

date_default_timezone_set("America/Santiago");

$loader = new Loader();
$loader->registerNamespaces(
    [
        'MyApp\Models' => __DIR__ . '/models/',
    ]
);
$loader->register();

$container = new FactoryDefault();
$container->set(
    'db',
    function () {
        return new PdoMysql(
            [
                'host'     => 'localhost',
                'username' => 'root',
                'password' => '',
                'dbname'   => 'innoapsion',
            ]
        );
    }
);

$app = new Micro($container);

//obtener el ultimo valor del bitcoin
$app->get(
    '/api/bitcoin',
    function () use ($app) {
        $phql = 'SELECT id, valor, fecha, peso FROM MyApp\Models\Bitcoin ORDER BY id DESC LIMIT 1';

        $bitcoin = $app
            ->modelsManager
            ->executeQuery($phql);
        $this->response->setHeader('Access-Control-Allow-Origin', '*');
        $aux = array(
            'valor' => $bitcoin[0]['valor'],
            'fecha' =>  date("d-m-Y H:i:s", strtotime($bitcoin[0]['fecha'])),
            'peso' => "$".number_format($bitcoin[0]['peso'],0, '','.'),
        );
        $this->response->setJsonContent($aux);
        return $this->response;
    }
);
//obtener el ultimos 2 registros
$app->get(
    '/api/last_bitcoin',
    function () use ($app) {
        $phql = 'SELECT id, valor, fecha, peso FROM  MyApp\Models\Bitcoin ORDER BY id DESC LIMIT 3';

        $bitcoins = $app
            ->modelsManager
            ->executeQuery($phql);
        $this->response->setHeader('Access-Control-Allow-Origin', '*');

        array_reverse($bitcoins);
        
        $aux = array();
        foreach($bitcoins as $bit){
            $aux2 = array(
                'valor' => $bit['valor'],
                'peso' => "$".number_format($bit['peso'],0, '','.'),
                'fecha' =>  date("d-m-Y H:i:s", strtotime($bit['fecha'])),
            );
            array_push( $aux, $aux2 );
        }
        unset($aux[0]);
        $aux = array_values($aux);
        $this->response->setJsonContent($aux);
        return $this->response;
    }
);


//guardar el bitcoin desde un cron
$app->get(
    '/api/save_bitcoin',
    function () use ($app) {
        $phql_api = 'SELECT value FROM MyApp\Models\Parametros WHERE name="api"';
        $phql_dolar = 'SELECT value FROM MyApp\Models\Parametros WHERE name="dollar"';
        $parametro_api = $app
            ->modelsManager
            ->executeQuery($phql_api);
        $parametro_dolar = $app
            ->modelsManager
            ->executeQuery($phql_dolar);

        $json = $parametro_api[0]['value'];
        $peso = $parametro_dolar[0]['value'];
        $valores = json_decode(file_get_contents($json), true);
        $valorBitcoin = $valores['bpi']['USD']['rate_float'];
        $peso_conversion = (float)$valorBitcoin * $peso;

        $response = new Response();

        if( $valorBitcoin == "" or $valorBitcoin < 1 ){
            $response->setStatusCode(500, 'Error');
            $response->setHeader('Access-Control-Allow-Origin', '*');
            return $response;
        }
        $phql  = 'INSERT INTO MyApp\Models\Bitcoin '
            . '(valor, fecha, peso) '
            . 'VALUES '
            . '(:valor:, :fecha:, :peso:)';
        
        $status = $app
            ->modelsManager
            ->executeQuery(
                $phql,
                [
                    'valor' => $valorBitcoin,
                    'fecha' => date('Y-m-d H:i:s'),
                    'peso' => number_format($peso_conversion, 0,'',''),
                ]
            );
        if ($status->success() === true) {
            $response->setStatusCode(201, 'Created');

            $bitcoin = $status->getModel();

            $response->setJsonContent(
                [
                    'status' => 'OK',
                    'data'   => $bitcoin,
                ]
            );
        } else {
            $response->setStatusCode(409, 'Conflict');

            $errors = [];
            foreach ($status->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }

            $response->setJsonContent(
                [
                    'status'   => 'ERROR',
                    'messages' => $errors,
                ]
            );
        }
        $response->setHeader('Access-Control-Allow-Origin', '*');

        return $response;
    }
);

$app->notFound(
    function () {
        global $app;
        $app->response->setStatusCode(404, "Not Found")->sendHeaders();
        echo 'Error!!';
    }
);

$app->handle(
    $_SERVER["REQUEST_URI"]
);