<?php

namespace MyApp\Models;

use Phalcon\Mvc\Model;
use Phalcon\Messages\Message;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\InclusionIn;

class Parametros extends Model
{
    public function validation()
    {
        $validator = new Validation();

        if ( empty($this->name)) {
            $this->appendMessage(
                new Message('El nombre no puede estar vacio')
            );
        }

        if ($this->validationHasFailed() === true) {
            return false;
        }
    }
}