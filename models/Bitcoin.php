<?php

namespace MyApp\Models;

use Phalcon\Mvc\Model;
use Phalcon\Messages\Message;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\InclusionIn;

class Bitcoin extends Model
{
    public function validation()
    {
        $validator = new Validation();

        if ( empty($this->fecha)) {
            $this->appendMessage(
                new Message('La fecha no puede estar vacia')
            );
        }

        if ($this->validationHasFailed() === true) {
            return false;
        }
    }
}